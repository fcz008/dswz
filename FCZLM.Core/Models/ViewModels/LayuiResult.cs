﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Web
{
    public class LayuiResult<T>
    {
        public int Code { get; set; } = 0;
        public string Msg { get; set; }
        public int Count { get; set; }
        public List<T> Data { get; set; }
    }
}
