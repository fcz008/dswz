﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace FCZLM.Core.Models
{
    public partial class ProductWeight
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int PositionId { get; set; }
        public string ProductWeight1 { get; set; }
        public int ProductUrl { get; set; }

        public virtual Position Position { get; set; }
        public virtual Products Product { get; set; }
        public virtual ProductUrls ProductUrlNavigation { get; set; }
    }
}