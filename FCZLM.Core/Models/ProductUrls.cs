﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace FCZLM.Core.Models
{
    public partial class ProductUrls
    {
        public ProductUrls()
        {
            LunBoTu = new HashSet<LunBoTu>();
            ProductWeight = new HashSet<ProductWeight>();
        }

        public int Id { get; set; }
        public string PicUrl { get; set; }
        public string PicType { get; set; }
        public DateTime CreateTime { get; set; }
        public int ProductId { get; set; }

        public virtual Products Product { get; set; }
        public virtual ICollection<LunBoTu> LunBoTu { get; set; }
        public virtual ICollection<ProductWeight> ProductWeight { get; set; }
    }
}