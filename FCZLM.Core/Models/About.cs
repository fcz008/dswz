﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace FCZLM.Core.Models
{
    public partial class About
    {
        public int Id { get; set; }
        public string Fzlc { get; set; }
        public string Fsjj { get; set; }
        public string Ppjs { get; set; }
        public string Hjxx { get; set; }
        public string Name { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
    }
}