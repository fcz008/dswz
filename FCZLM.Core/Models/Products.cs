﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace FCZLM.Core.Models
{
    public partial class Products
    {
        public Products()
        {
            OrderDetails = new HashSet<OrderDetails>();
            ProductMessage = new HashSet<ProductMessage>();
            ProductUrls = new HashSet<ProductUrls>();
            ProductWeight = new HashSet<ProductWeight>();
            ShoppingCart = new HashSet<ShoppingCart>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public string Desc { get; set; }
        public string Brand { get; set; }
        public int Status { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public int ProductCategoryId { get; set; }
        public decimal? Ext1 { get; set; }
        public string Ext2 { get; set; }

        public virtual ProductCategorys ProductCategory { get; set; }
        public virtual ICollection<OrderDetails> OrderDetails { get; set; }
        public virtual ICollection<ProductMessage> ProductMessage { get; set; }
        public virtual ICollection<ProductUrls> ProductUrls { get; set; }
        public virtual ICollection<ProductWeight> ProductWeight { get; set; }
        public virtual ICollection<ShoppingCart> ShoppingCart { get; set; }
    }
}