﻿using FCZLM.Application.Service;
using FCZLM.Application.Service.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using FCZLM.Core.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks; 

namespace FCZLM.Areas.Controllers
{
    public class FAboutController : Controller
    {

        private readonly IRepository<About, int> _aboutRepository;
        private readonly IAboutService _AboutRepository;
        public FAboutController(IRepository<About, int> aboutRepository, IAboutService AboutRepository)
        {
            _aboutRepository = aboutRepository;
            _AboutRepository = AboutRepository;
        }
        #region 关于我们列表
        [HttpGet]
        public async Task<IActionResult> Index(int id = 1)//需要传递主键
        {
            //返回序列中的第一个元素；如果未找到该元素，则返回默认值。
            About aboutus = await _aboutRepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(aboutus);//强类型传值
        }
        [HttpPost]
        /// <summary>
        /// 获取关于我们信息修改
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Index(About about)//封装
        {
            About model = await _aboutRepository.FirstOrDefaultAsync(x => x.Id == about.Id);
            string msg = "修改失败，请重试！";
            bool isSuccess = false;//后台是否添加成功，默认是失败
            //进行修改操作
            if (ModelState.IsValid)
            {
                model.Name = about.Name;
                model.Tel = about.Tel;
                model.Email = about.Email;
                model.Address = about.Address;
                model.Fsjj = about.Fsjj;
                model.Ppjs = about.Ppjs;
                model.Fzlc = about.Fzlc;
                model.Hjxx = about.Hjxx;
                await _aboutRepository.UpdateAsync(model);
                isSuccess = true;
                msg = "修改成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
        #region 关于我们修改
        [HttpGet]
        public async Task<IActionResult> Edits(int id)
        {

            About about = await _aboutRepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(about);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(About about)
        {
            About model = await _aboutRepository.FirstOrDefaultAsync(x => x.Id == about.Id);
            string msg = "修改失败，请重试";
            bool isSuccess = false;//看后台是否修改成功，默认为失败
            if (ModelState.IsValid)
            {
                model.Fzlc = about.Fzlc;
                model.Fsjj = about.Fsjj;
                model.Ppjs = about.Ppjs;
                model.Hjxx = about.Hjxx;
                model.Name = about.Name;
                model.Tel = about.Tel;
                model.Address = about.Address;
                model.Email = about.Email;
                await _aboutRepository.UpdateAsync(model);
                isSuccess = true;
                msg = "修改成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion

    }
}
