﻿using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace FCZLM.Areas.Controllers
{
    public class FNewsController : Controller
    {
        private readonly ILogger<FNewsController> _logger;
        private readonly INewService _newService;
        private readonly IRepository<News, int> _newsRepository;
        private readonly IRepository<NewCategorys, int> _newsCategoryRepository;
        private readonly INewService _NewsRepository;
        public FNewsController(ILogger<FNewsController> logger, IRepository<News, int> newsRepository, 
            INewService NewsRepository,IRepository<NewCategorys,int> newsCategoryRepository)
        {
            _logger = logger;
            _newsRepository = newsRepository;
            _NewsRepository = NewsRepository;
            _newsCategoryRepository = newsCategoryRepository;
        }
        #region//新闻列表
        [HttpGet]
        public IActionResult Index()
        {
            var CategoryRepository = _newsCategoryRepository.GetAllList();
            ViewBag._newsCategoryRepository = CategoryRepository;
           return View();
        }
        [HttpPost]
        public IActionResult Index(NewInput input)
        {
            var list = _NewsRepository.GetPaginatedResult(input);
            return Json(new Core.ViewModel.LayuiResult<NewModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });

           

        }
        #endregion

        #region//新闻添加
        [HttpGet]
        public IActionResult Add() 
        {
            //var con=_newsRepository.GetAllListAsync();
            var list = _newsCategoryRepository.GetAllList();
            ViewBag._newsCategoryRepository = list;
            return View();
        }
        [HttpPost]
        public IActionResult Add(News news)
        {
            news.CreateTime = DateTime.Now;
            bool isSuccess = false;//后台是否添加成功，默认是失败
            string msg = "添加失败，请重试！";
            if (ModelState.IsValid)
            {
                isSuccess = true;
                msg = "添加成功，开始跳转！";
                _newsRepository.Insert(news);
            }
            return Json(new { isSuccess, msg });

        }
        #endregion

        #region//新闻修改
        [HttpGet]
        public async Task <IActionResult> Edits(int id)
        {
            var list = _newsCategoryRepository.GetAllList();
            ViewBag._newsCategoryRepository = list;
            News news = await _newsRepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(news);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(News news)
        {
            
            News model = await _newsRepository.FirstOrDefaultAsync(x => x.Id == news.Id);
            string msg = "修改失败，请重试";
            bool isSuccess = false;//看后台是否修改成功，默认为失败
            if (ModelState.IsValid)
            {
                model.Title = news.Title;
                model.SubTitle = news.SubTitle;
                model.Summary = news.Summary;
                model.Content = news.Content;
                model.Clicks = news.Clicks;
                model.Status = news.Status;
                model.Source = news.Source;
                model.NewCategoryId= news.NewCategoryId;
                await _newsRepository.UpdateAsync(model);
                isSuccess = true;
                msg = "修改成功，开始刷新！";
            }
            return Json(new {isSuccess,msg});
        }
        #endregion

        #region//新闻删除
        [HttpPost]
        public IActionResult Del(int id)//主键列传递过来
        {
            News p = new News() { Id = id };
            string msg = "删除失败，请重试！";
            bool isSuccess = false;//后台是否添加成功，默认是失败
            if (ModelState.IsValid)
            {
                _newsRepository.Delete(p);
                isSuccess = true;
                msg = "删除成功，页面开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
    }
}
