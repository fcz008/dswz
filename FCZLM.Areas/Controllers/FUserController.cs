﻿using FCZLM.Application.Service;
using FCZLM.Application.Service.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FCZLM.Areas.Controllers
{
    public class FUserController : Controller
    {
        private readonly IRepository<Users, int> _userRepository;
        private readonly IUserService _UserRepository;
        public FUserController(IRepository<Users, int> userRepository, IUserService UserRepository)
        {
            _userRepository = userRepository;
            _UserRepository = UserRepository;
        }
        [HttpGet]
        public IActionResult Index()
        {
            var user = _userRepository.GetAllList();
            ViewBag._messageRepository = user;
            return View();
        }
        [HttpPost]
        public IActionResult Index(UserInput input)
        {
            var list = _UserRepository.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<UserModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
    }
}

