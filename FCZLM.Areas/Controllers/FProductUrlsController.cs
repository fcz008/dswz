﻿using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace FCZLM.Areas.Controllers
{
    public class FProductUrlsController : Controller
    {

        private readonly IProductUrlService _productUrlService;
        private readonly IRepository<ProductUrls, int> _producturlsRepository;
        private readonly IRepository<Products, int> _productsRepository;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public FProductUrlsController(IProductUrlService productUrlService, IRepository<ProductUrls, int> producturlsRepository, IRepository<Products, int> productsRepository,IWebHostEnvironment hostingEnvironment)
        {
            _productUrlService = productUrlService;
            _producturlsRepository = producturlsRepository;
            _productsRepository = productsRepository;
            _hostingEnvironment = hostingEnvironment;
        }
        #region 商品图片列表
        [HttpGet]
        public IActionResult Index()
        {
            var products = _producturlsRepository.GetAll();
            ViewBag._producturlsRepository = products;
            return View();
        }
        [HttpPost]
        public IActionResult Index(ProductUrlInput input)
        {
            var list = _productUrlService.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<ProductUrlModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
        #endregion
        #region 商品图片添加
        public IActionResult Add()
        {
            //var con=_newsRepository.GetAllListAsync();
            var list = _productsRepository.GetAllList();
            ViewBag._productsRepository = list;          
            return View();
        }
        [HttpPost]
        public IActionResult Add(ProductUrls productUrls)
        {

            bool isSuccess = false;//后台是否添加成功，默认是失败
            string msg = "添加失败，请重试！";
            if (ModelState.IsValid)
            {
                isSuccess = true;
                msg = "添加成功，开始跳转！";
                productUrls.CreateTime = DateTime.Now;
                _producturlsRepository.Insert(productUrls);
            }
            return Json(new { isSuccess, msg });

        }
        public IActionResult UploadImg(IFormFile PicUrl1)
        {
            //1、获取图片名称
            string imgName = PicUrl1.FileName;
            //2、获取images路径
            //string path = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\images"}";
            string path = _hostingEnvironment.WebRootPath;
            //3、保存图片到images
            //file.SaveAs(path+imgName);
            using (var stream = new FileStream(Path.Combine(path, "images/", imgName), FileMode.Create, FileAccess.Write))
            {
                PicUrl1.CopyTo(stream);
            }
            return Json(new { code = 0, msg = "上传成功", path = "/images/" + imgName });
        }
        #endregion
        #region 商品图片修改
        public async Task<IActionResult> Edits(int id)
        {
            //var con=_newsRepository.GetAllListAsync();
            var list = _productsRepository.GetAllList();
            ViewBag._productsRepository = list;
            ProductUrls productUrls = await _producturlsRepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(productUrls);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(ProductUrls productUrls)
        {
            ProductUrls model = await _producturlsRepository.FirstOrDefaultAsync(x => x.Id == productUrls.Id);
            bool isSuccess = false;//后台是否添加成功，默认是失败
            string msg = "修改失败，请重试";
           
            if (ModelState.IsValid)
            {
                model.PicUrl = productUrls.PicUrl;
                model.PicType = productUrls.PicType;
                model.CreateTime = DateTime.Now;    
                model.ProductId = productUrls.ProductId;
                await _producturlsRepository.UpdateAsync(model);
                isSuccess = true;
                msg = "修改成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });

        }

        #endregion
        #region 商品图片删除
        [HttpPost]
        public IActionResult Del(int id)//主键列传递过来
        {
            ProductUrls p = new ProductUrls() { Id = id };
            string msg = "删除失败，请重试！";
            bool isSuccess = false;//后台是否添加成功，默认是失败
            if (ModelState.IsValid)
            {
                _producturlsRepository.Delete(p);
                isSuccess = true;
                msg = "删除成功，页面开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
    }
}
