﻿using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FCZLM.Areas.Controllers
{
    public class FProductCategorysController : Controller
    {
        private readonly ILogger<FProductCategorysController> _logger;
        private readonly IProductCategoryService _productCategoryService;
        private readonly IRepository<ProductCategorys, int> _productcategorysRepository;
        private readonly IProductCategoryService _ProductCategoryService;
        public FProductCategorysController(ILogger<FProductCategorysController> logger, IRepository<ProductCategorys, int> productcategoryRepository,
            IProductCategoryService ProductCategoryService)
        {
            _logger = logger;
            _productcategorysRepository = productcategoryRepository;
            _ProductCategoryService = ProductCategoryService;
        }
        #region 商品分类列表
        [HttpGet]
        public IActionResult Index()
        {
            var NewCategory = _productcategorysRepository.GetAllList();
            ViewBag.NewsCategory = NewCategory;
            return View();
        }
        [HttpPost]
        public IActionResult Index(ProductCategoryInput input)
        {
            var list = _ProductCategoryService.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<ProductCategoryModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
        #endregion
        #region 商品分类添加
        [HttpGet]
        public IActionResult Add()
        {
            //var con=_newsRepository.GetAllListAsync();
            var list = _productcategorysRepository.GetAllList();
            ViewBag._productcategorysRepository = list;
            return View();
        }
        [HttpPost]
        public IActionResult Add(ProductCategorys productCategorys)
        {

            bool isSuccess = false;//后台是否添加成功，默认是失败
            string msg = "添加失败，请重试！";
            if (ModelState.IsValid)
            {
                isSuccess = true;
                msg = "添加成功，开始跳转！";
                _productcategorysRepository.Insert(productCategorys);
            }
            return Json(new { isSuccess, msg });

        }
        #endregion
        #region 商品分类修改
        [HttpGet]
        public async Task<IActionResult> Edits(int id)
        {

            ProductCategorys newCategorys = await _productcategorysRepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(newCategorys);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(ProductCategorys productCategorys)
        {
            ProductCategorys model = await _productcategorysRepository.FirstOrDefaultAsync(x => x.Id == productCategorys.Id);
            string msg = "修改失败，请重试";
            bool isSuccess = false;//看后台是否修改成功，默认为失败
            if (ModelState.IsValid)
            {
                model.Name = productCategorys.Name;
                model.Desc = productCategorys.Desc;
                await _productcategorysRepository.UpdateAsync(model);
                isSuccess = true;
                msg = "修改成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
        #region 商品分类删除
        [HttpPost]
        public IActionResult Del(int id)//主键列传递过来
        {
            ProductCategorys p = new ProductCategorys() { Id = id };
            string msg = "删除失败，请重试！";
            bool isSuccess = false;//后台是否添加成功，默认是失败
            if (ModelState.IsValid)
            {
                _productcategorysRepository.Delete(p);
                isSuccess = true;
                msg = "删除成功，页面开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
    }
}
