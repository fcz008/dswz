﻿
using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Application.Service;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using XProject.Service;

namespace FCZLM.Areas.Controllers
{
    public class FAdminController : Controller
    {
        private readonly IRepository<Admin, int> _adminRepository;
        private readonly IAdminsService _AdminRepository;
        public FAdminController(IRepository<Admin, int> adminRepository, IAdminsService AdminRepository)
        {
            _adminRepository = adminRepository;
            _AdminRepository = AdminRepository;
        }
        #region 关于我们列表
        [HttpGet]
        public IActionResult Index()
        {
            var Admin = _adminRepository.GetAllList();
            ViewBag._adminRepository = Admin;
            return View();
        }
        [HttpPost]
        public IActionResult Index(AdminsInput input)
        {
            var list = _AdminRepository.GetPaginatedResult( input);
            return Json(new Web.LayuiResult<AdminsModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
        #endregion
        #region 关于我们修改
        [HttpGet]
        public async Task<IActionResult> Edits(int id)
        {

            Admin admin = await _adminRepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(admin);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(Admin admin)
        {
            Admin model = await _adminRepository.FirstOrDefaultAsync(x => x.Id == admin.Id);
            string msg = "修改失败，请重试";
            bool isSuccess = false;//看后台是否修改成功，默认为失败
            if (ModelState.IsValid)
            {
                model.LoginName = admin.LoginName;
                model.LoginPwd = admin.LoginPwd;
                await _adminRepository.UpdateAsync(model);
                isSuccess = true;
                msg = "修改成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
    }
}
