﻿using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FCZLM.Areas.Controllers
{
    public class FMessageController : Controller
    {
        private readonly IRepository<Messages, int> _messageRepository;
        private readonly IRepository<Users, int> _userRepository;
        private readonly IMessafeService _MessageRepository;

        public FMessageController(IRepository<Messages, int> messageRepository, IMessafeService MessageRepository, IRepository<Users, int> userRepository)
        { 
            _messageRepository = messageRepository;
            _MessageRepository = MessageRepository;
            _userRepository = userRepository;
        }
        #region 留言列表
        [HttpGet]
        public IActionResult Index()
        {
            var CategoryRepository = _userRepository.GetAllList();
            ViewBag._userRepository = CategoryRepository;
            return View();
        }
        [HttpPost]
        public IActionResult Index(MessageInput input)
        {
            var list = _MessageRepository.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<MessageModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
        #endregion
        #region 留言回复
        [HttpGet]
        public async Task<IActionResult> Edits(int id)
        {
            
            Messages messages = await _messageRepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(messages);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(Messages messages)
        {

            Messages model = await _messageRepository.FirstOrDefaultAsync(x => x.Id == messages.Id);
            string msg = "回复失败，请重试";
            bool isSuccess = false;//看后台是否修改成功，默认为失败
            if (ModelState.IsValid)
            {
                //model.UserId = messages.UserId;
                //model.Ask = messages.Ask;
                //model.AskTime = DateTime.Now;
                //model.Phone = messages.Phone;
                model.Huifu =messages.Huifu;
                await _messageRepository.UpdateAsync(model);
                isSuccess = true;
                msg = "回复成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }

        #endregion
    }
}

