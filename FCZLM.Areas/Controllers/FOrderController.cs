﻿using Microsoft.AspNetCore.Mvc;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.ViewModel;
using System.Threading.Tasks;
using System.Linq;

namespace FCZLM.Areas.Controllers
{
    public class FOrderController : Controller
    {
        private readonly IRepository<Orders, int> _orerrepository;
        private readonly IOrderService _orderService;
        private readonly IRepository<Users, int> _usersRepository;
        private readonly IRepository<OrderDetails, int> _orderDetailsRepository;
        private readonly IRepository<Products, int> _productsRepository;

        public FOrderController(IRepository<Orders, int> orerrepository, IOrderService orderService, IRepository<Users, int> usersRepository, IRepository<OrderDetails, int> orderDetailsRepository, IRepository<Products, int> productsRepository)
        {

            _orerrepository = orerrepository;
            _orderService = orderService;
            _usersRepository = usersRepository;
            _orderDetailsRepository = orderDetailsRepository;
            _productsRepository = productsRepository;
        }
        #region 订单列表
        [HttpGet]
        public IActionResult Index()
        {
            var usersRepository = _usersRepository.GetAllList();
            ViewBag._usersRepository = usersRepository;
            return View();
        }
        [HttpPost]
        public IActionResult Index(OrderInput input)
        {
            var list = _orderService.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<OrderModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
        #endregion
        #region 订单修改
        [HttpGet]
        public async Task<IActionResult> Edits(int id)
        {
            
            Orders orders = await _orerrepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(orders);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(Orders orders)
        {
            Orders model = await _orerrepository.FirstOrDefaultAsync(x => x.Id == orders.Id);
            string msg = "修改失败，请重试";
            bool isSuccess = false;//看后台是否修改成功，默认为失败
            if (ModelState.IsValid)
            {
                model.TotalMoney = orders.TotalMoney;
                await _orerrepository.UpdateAsync(model);
                isSuccess = true;
                msg = "修改成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
        #region 订单详情
        public IActionResult Index2(int id)
        {
            var orderDetails = _orderDetailsRepository.GetAllList().FirstOrDefault(x=>x.OrderId==id);
            ViewBag._orderDetailsRepository = orderDetails;
            var user = _usersRepository.GetAllList();
            ViewBag._userRepository = user;
            var products = _productsRepository.FirstOrDefault(x=>x.Id== orderDetails.ProductId);
            ViewBag._productsRepository = products;
            return View(orderDetails);
        }
        #endregion

    }
}