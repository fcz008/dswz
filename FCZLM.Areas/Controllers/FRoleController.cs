﻿using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FCZLM.Areas.Controllers
{

    public class FRoleController : Controller
    {
        private readonly IRepository<Roles, int> _rolerepository;
        private IRoleService _roleService;
        public FRoleController(IRepository<Roles, int> rolerepository, IRoleService roleService)
        {
            _rolerepository = rolerepository;
            _roleService = roleService;
        }
        #region 角色列表
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(RoleInput input)
        {
            var list = _roleService.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<RoleModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
        #endregion
        #region 角色添加
        [HttpGet]
        public IActionResult Add()
        {
            //var con=_newsRepository.GetAllListAsync();
            var list = _rolerepository.GetAllList();
            ViewBag._rolerepository = list;
            return View();
        }
        [HttpPost]
        public IActionResult Add(Roles roles)
        {

            bool isSuccess = false;//后台是否添加成功，默认是失败
            string msg = "添加失败，请重试！";
            if (ModelState.IsValid)
            {
                isSuccess = true;
                msg = "添加成功，开始跳转！";
                _rolerepository.Insert(roles);
            }
            return Json(new { isSuccess, msg });

        }
        #endregion
    }
}
