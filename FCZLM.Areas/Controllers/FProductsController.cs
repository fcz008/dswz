﻿using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FCZLM.Areas.Controllers
{
    public class FProductsController : Controller
    {
        private readonly ILogger<FProductsController> _logger;
        private readonly IProductService _productsService;
        private readonly IRepository<Products, int> _productsRepository;
        private readonly IRepository<ProductCategorys, int> _productCategorysRepository;
        private readonly IRepository<ProductUrls, int> _productUrlsRepository;
        private readonly IProductService _ProductRepository;
        public FProductsController(ILogger<FProductsController> logger, IRepository<Products, int> productsRepository,
            IProductService ProductRepository, IRepository<ProductCategorys, int> productCategorysRepository, IRepository<ProductUrls, int> productUrlsRepository)
        {
            _logger = logger;
            _productsRepository = productsRepository;
            _ProductRepository = ProductRepository;
            _productCategorysRepository = productCategorysRepository;
            _productUrlsRepository = productUrlsRepository;
        }
        #region 商品列表
        [HttpGet]
        public IActionResult Index()
        {
            var CategoryRepository = _productCategorysRepository.GetAllList();
            ViewBag._productCategorysRepository = CategoryRepository;
            return View();
        }
        [HttpPost]
        public IActionResult Index(ProductInput input)
        {
            var list = _ProductRepository.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<ProductModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
        #endregion
        #region 商品添加
        [HttpGet]
        public IActionResult Add()
        {
            //var con=_newsRepository.GetAllListAsync();
            var list = _productCategorysRepository.GetAllList();
            ViewBag._productCategorysRepository = list;
            return View();
        }
        [HttpPost]
        public IActionResult Add(Products products)
        {
            products.UpdateTime = DateTime.Now;
            products.CreateTime = DateTime.Now;
            bool isSuccess = false;//后台是否添加成功，默认是失败
            string msg = "添加失败，请重试！";
            if (ModelState.IsValid)
            {
                isSuccess = true;
                msg = "添加成功，开始跳转！";
                _productsRepository.Insert(products);
            }
            return Json(new { isSuccess, msg });

        }
        #endregion
        #region 商品修改
        [HttpGet]
        public async Task<IActionResult> Edits(int id)
        {
            var list = _productCategorysRepository.GetAllList();
            ViewBag._productCategorysRepository = list;
            Products products = await _productsRepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(products);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(Products products)
        {
            Products model = await _productsRepository.FirstOrDefaultAsync(x => x.Id == products.Id);
            string msg = "修改失败，请重试";
            bool isSuccess = false;//看后台是否修改成功，默认为失败
            if (ModelState.IsValid)
            {
                model.Name = products.Name;
                model.Price = products.Price;
                model.Desc = products.Desc;
                model.Brand = products.Brand;
                model.Status = products.Status;
                model.ProductCategoryId = products.ProductCategoryId;
                model.UpdateTime = DateTime.Now;
                await _productsRepository.UpdateAsync(model);
                isSuccess = true;
                msg = "修改成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
        #region 商品详情
        public IActionResult Index2(int id)
        {
            var producturls = _productUrlsRepository.GetAllList().FirstOrDefault(x => x.ProductId == id);
            ViewBag._productUrlsRepository = producturls;
           
            return View(producturls);
        }
        #endregion
        #region 商品删除
        [HttpPost]
        public IActionResult Del(int id)//主键列传递过来
        {
            Products p = new Products() { Id = id };
            string msg = "删除失败，请重试！";
            bool isSuccess = false;//后台是否添加成功，默认是失败
            if (ModelState.IsValid)
            {
                _productsRepository.Delete(p);
                isSuccess = true;
                msg = "删除成功，页面开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
    }
}
