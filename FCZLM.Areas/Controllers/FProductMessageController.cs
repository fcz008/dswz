﻿using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FCZLM.Areas.Controllers
{
    public class FProductMessageController : Controller
    {
       
        private readonly IProductMessageService _productmessageService;
        private readonly IRepository<ProductMessage, int> _productmessageRepository;
        private readonly IRepository<Products, int> _productRepository;
        private readonly IRepository<Orders, int> _orderRepository;
      
        public FProductMessageController(IRepository<ProductMessage, int> productmessageRepository,
            IProductMessageService productmessageService, IRepository<Products, int> productRepository, IRepository<Orders, int> orderRepository)
        {
            _orderRepository = orderRepository;
            _productmessageRepository = productmessageRepository;
            _productmessageService = productmessageService;
            _productRepository = productRepository;
        }
        #region 商品评论列表
        [HttpGet]
        public IActionResult Index()
        {
            var Product = _productRepository.GetAllList();
            ViewBag._productRepository = Product;
            var Order = _orderRepository.GetAllList();
            ViewBag._orderRepository = Order;
            return View();
        }
        [HttpPost]
        public IActionResult Index(ProductMessageInput input)
        {
            var list = _productmessageService.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<ProdutMessageModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
        #endregion
        #region 商品评论回复
        [HttpGet]
        public async Task<IActionResult> Edits(int id)
        {

            ProductMessage productMessage = await _productmessageRepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(productMessage);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(ProductMessage productMessage)
        {

            ProductMessage model = await _productmessageRepository.FirstOrDefaultAsync(x => x.Id == productMessage.Id);
            string msg = "回复失败，请重试";
            bool isSuccess = false;//看后台是否修改成功，默认为失败
            if (ModelState.IsValid)
            {
                //model.UserId = messages.UserId;
                //model.Ask = messages.Ask;
                //model.AskTime = DateTime.Now;
                //model.Phone = messages.Phone;
                model.Huifu = productMessage.Huifu;
                await _productmessageRepository.UpdateAsync(model);
                isSuccess = true;
                msg = "回复成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }

        #endregion
    }
}
