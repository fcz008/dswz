﻿using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using FCZLM.Core.Models;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using System.Threading.Tasks;

namespace FCZLM.Areas.Controllers
{
    public class FNewsCategorysController : Controller
    {
        private readonly ILogger<FNewsCategorysController> _logger;
        private readonly INewCategoryService _newsCategoryService;
        private readonly IRepository<NewCategorys, int> _newcategorysrepository;
        private readonly INewCategoryService _NewCategoryService;
        public FNewsCategorysController(ILogger<FNewsCategorysController> logger, IRepository<NewCategorys, int> newcategorysrepository,
            INewCategoryService NewCategoryService)
        {
            _logger = logger;
            _newcategorysrepository = newcategorysrepository;
            _NewCategoryService= NewCategoryService;
        }
        #region 新闻分类列表
        [HttpGet]
        public IActionResult Index()
        {
            var NewCategory = _newcategorysrepository.GetAllList();
            ViewBag.NewsCategory = NewCategory;
            return View();
        }
        [HttpPost]
        public IActionResult Index(NewCategoryInput input)
        {
            var list = _NewCategoryService.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<NewCategoryModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
        #endregion
        #region 新闻分类添加
        [HttpGet]
        public IActionResult Add()
        {
            //var con=_newsRepository.GetAllListAsync();
            var list = _newcategorysrepository.GetAllList();
            ViewBag._newcategorysrepository = list;
            return View();
        }
        [HttpPost]
        public IActionResult Add(NewCategorys newCategorys)
        {

            bool isSuccess = false;//后台是否添加成功，默认是失败
            string msg = "添加失败，请重试！";
            if (ModelState.IsValid)
            {
                isSuccess = true;
                msg = "添加成功，开始跳转！";
                _newcategorysrepository.Insert(newCategorys);
            }
            return Json(new { isSuccess, msg });

        }
        #endregion
        #region 新闻分类修改
        [HttpGet]
        public async Task<IActionResult> Edits(int id)
        {
           
            NewCategorys newCategorys = await _newcategorysrepository.FirstOrDefaultAsync(x => x.Id == id);
            return View(newCategorys);
        }
        [HttpPost]
        public async Task<IActionResult> Edits(NewCategorys newCategorys)
        {
            NewCategorys model = await _newcategorysrepository.FirstOrDefaultAsync(x => x.Id == newCategorys.Id);
            string msg = "修改失败，请重试";
            bool isSuccess = false;//看后台是否修改成功，默认为失败
            if (ModelState.IsValid)
            {
                model.Name = newCategorys.Name;
                model.Desc = newCategorys.Desc;
                await _newcategorysrepository.UpdateAsync(model);
                isSuccess = true;
                msg = "修改成功，开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
        #region 新闻分类删除
        [HttpPost]
        public IActionResult Del(int id)//主键列传递过来
        {
            NewCategorys p = new NewCategorys() { Id = id };
            string msg = "删除失败，请重试！";
            bool isSuccess = false;//后台是否添加成功，默认是失败
            if (ModelState.IsValid)
            {
                _newcategorysrepository.Delete(p);
                isSuccess = true;
                msg = "删除成功，页面开始刷新！";
            }
            return Json(new { isSuccess, msg });
        }
        #endregion
    }
}
