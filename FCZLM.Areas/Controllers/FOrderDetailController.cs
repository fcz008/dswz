﻿using FCZLM.Application.HTService;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FCZLM.Areas.Controllers
{
    public class FOrderDetailController : Controller
    {

        private readonly IRepository<OrderDetails, int> _OrderDetailsrepository;
        private readonly IOrderDetailService _OrderDetailsService;
        private readonly IRepository<Orders, int> _OrdersRepository;

        public FOrderDetailController(IRepository<OrderDetails, int> OrderDetailsrepository, IRepository<Orders, int> OrdersRepository, IOrderDetailService OrderDetailsService)
        {
            _OrderDetailsrepository = OrderDetailsrepository;
            _OrderDetailsService = OrderDetailsService;
            _OrdersRepository = OrdersRepository;
        }
        [HttpGet]
        public IActionResult Index()
        {
            var OrdersRepository = _OrdersRepository.GetAllList();
            ViewBag._OrdersRepository = OrdersRepository;
            return View();
        }
        [HttpPost]
        public IActionResult Index(OrderDetailInput input)
        {
            var list = _OrderDetailsService.GetPaginatedResult(input);
            return Json(new Web.LayuiResult<OrderDetailModel> { Code = 0, Msg = "", Data = list.Data, Count = list.TotalCount });
        }
    }
}
