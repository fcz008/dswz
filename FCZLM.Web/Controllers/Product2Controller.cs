﻿using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
namespace FCZLM.Web.Controllers
{

    public class Product2Controller : Controller
    {
        private readonly IRepository<Products, int> _ProductsRepository;
        private readonly IRepository<ProductCategorys, int> _ProductCategorysRepository;
        private readonly IRepository<ProductUrls, int> _ProductUrlsRepository;
        public Product2Controller(ILogger<ProductController> logger, IRepository<Products, int> ProductsRepository,
         IRepository<ProductCategorys, int> ProductCategorysRepository, IRepository<ProductUrls, int> ProductUrlsRepository
         )
        {
            _ProductsRepository = ProductsRepository;
            _ProductCategorysRepository = ProductCategorysRepository;
            _ProductUrlsRepository = ProductUrlsRepository;


        }
        public IActionResult Index(int id)
        {
            var proList = _ProductsRepository.GetAll().Include(x=>x.ProductUrls).Where(x=>x.ProductCategoryId==id).ToList();
            var procategory = _ProductCategorysRepository.GetAllList();
            //var prourl = _ProductUrlsRepository.GetAllList();
            ViewBag.ProductCategory = procategory;
           // ViewBag.ProductUrl = prourl;
            //ViewBag.Product = pro;
            return View(proList);
        }
    }
}
