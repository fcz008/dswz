﻿using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FZCLM.Web.Controllers
{
    public class AboutController : Controller
    {
        private readonly IRepository<About, int> _aboutRepository;

        public AboutController(IRepository<About, int> aboutRepository)
        {
            _aboutRepository = aboutRepository;
        }
        public IActionResult Index()
        {
            var about = _aboutRepository.FirstOrDefault(x=>1==1);
            
            return View(about);
        }
    }
}
