﻿using FCZLM.Application.Service.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;


namespace FZCLM.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly IRepository<Users, int> _userRepository;

        public LoginController(IRepository<Users, int> userRepository)
        {
            _userRepository = userRepository;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(string UserName, string UserPwd)
        {
            var user = _userRepository.FirstOrDefault(x => x.UserName == UserName);
            string msg="用户名或者密码错误，请重试！";
            bool isSuccess=false;
            if (user != null && user.UserPwd == UserPwd)
            {
                HttpContext.Session.SetString("user", user.UserName);
                msg = "登陆成功";
                isSuccess = true;
               
                return Json(new { msg, isSuccess });
            }
            else
            {
                msg = "用户名或者密码错误";
                isSuccess = false;
                return Json(new { msg, isSuccess });
            }

        }
        
        public IActionResult ZhuCe()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ZhuCe(Users users)
        {
            var User = _userRepository.FirstOrDefault(x=>x.UserName==users.UserName);
            string msg;
            bool isSuccess;
            if (User==null)
            {
                users.UserTime = DateTime.Now;
                msg = "注册成功，即将跳转登录";
                _userRepository.Insert(users);
                isSuccess = true;

                return Json(new { msg, isSuccess });
               
                
            }
            else
            {
                msg = "该用户名已存在";
                isSuccess = false;
                return Json(new { msg, isSuccess });
            }

        }
    }
}