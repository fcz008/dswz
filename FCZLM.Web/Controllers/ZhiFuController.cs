﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Http;
using Aop.Api;
using Aop.Api.Request;
using Aop.Api.Domain;
using Aop.Api.Response;
using Address = FCZLM.Core.Models.Address;
using System.Linq;

namespace FCZLM.Web.Controllers
{
    public class ZhiFuController : Controller
    {
        private readonly IRepository<Users, int> _userRepository;
        private readonly IRepository<Address, int> _addressRepository;
        private readonly IRepository<Orders, int> _orderRepository;
        private readonly IRepository<OrderDetails, int> _orderdetailsRepository;
        private readonly IRepository<Products, int> _productRepository;
        private readonly IRepository<ProductUrls, int> _producturlRepository;

        public ZhiFuController(IRepository<Users, int> userRepository,IRepository<Address, int> addressRepository, IRepository<Orders, int> orderRepository, IRepository<Products, int> productRepository, IRepository<ProductUrls, int> producturlRepository,IRepository<OrderDetails,int> orderdetailsrepository)
        {
            _userRepository = userRepository;
            _addressRepository = addressRepository;
            _orderRepository = orderRepository;
            _productRepository = productRepository;
            _producturlRepository = producturlRepository;
            _orderdetailsRepository = orderdetailsrepository;
        }
       
        public IActionResult Index(int Id)
        {
            var a = _productRepository.GetAll().FirstOrDefault(x => x.Id == Id);
            double b = (double)(a.Price);
            string app_id = "2021000122686359";
            string alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAi0rKfULdS7bGwDL8ITJVCVkH/D5d8jz+Nlz8dxtr3BB3CMq1pHyk7OhnCRWZu902LjOpV7lc2VS7zAMvHz6I+/KHCwdncH79EeSIBwKgt+WoeWGhBCLfw6AsTlnno9Ub9uBVhnQ6b2HV7FfOogPM32XNuugZXPjXHMtW6yjdrQoLGVh0D7JmP3vxLMuPAL4HefVWtO1ycr8moqFHFtLM9L+zNVj87fBk0kFwox87NwDkfMZgvxAEbhKPANVc8c7SiOIflbiRrmjdJxIi6wlIo9yS/fZX12cLQuiDNSvCqRUIu/T0t96c3tiSCBwvAz7XsBF8ZHIizrTjzPSQlAhUtQIDAQAB";
            string merchant_private_key = "MIIEogIBAAKCAQEAi0rKfULdS7bGwDL8ITJVCVkH/D5d8jz+Nlz8dxtr3BB3CMq1pHyk7OhnCRWZu902LjOpV7lc2VS7zAMvHz6I+/KHCwdncH79EeSIBwKgt+WoeWGhBCLfw6AsTlnno9Ub9uBVhnQ6b2HV7FfOogPM32XNuugZXPjXHMtW6yjdrQoLGVh0D7JmP3vxLMuPAL4HefVWtO1ycr8moqFHFtLM9L+zNVj87fBk0kFwox87NwDkfMZgvxAEbhKPANVc8c7SiOIflbiRrmjdJxIi6wlIo9yS/fZX12cLQuiDNSvCqRUIu/T0t96c3tiSCBwvAz7XsBF8ZHIizrTjzPSQlAhUtQIDAQABAoIBAGoQTjWpZ3aSu654ZDIm8EeI6qKxHroopW3hykAruiq8wtcH+94/JxpaMLLjJse9PtdZcTYiKEg41DMZrZT9+jLWzuuf/KzVlYA9YzLrVlQRZQLgS0op//cnDPWwUckXqg6HbbhO4nwTpGAt0GLfv+3PyCLtPvrjZHFBiHLSEFIEshgugUX9iNBbuNz63kZHHRPq9k3X1T5liWvyJf/KksFsvDpfrHOhY5FLmIm42GX3IJxlEhICR2nzW8UBFDxbBQ82rsCd/soTvtfzifppUsS/yulsBQwoL2HimaYl1HkhmuruvBeBW433iMq+sXQ3eGNTIGBMrebowVrVU1190vUCgYEA4+DifPHCq5eJo99Fv3AQCmMSLabLlH/dFX9cIQ8Ss0/fMENRjgA9BNno5e4Bunhj/FIqg2/nXXeRRHk6GwIW6VU82PtIODuWRp571SJmjjHaK7qCYKCNPXm3m0jYZqRuYBis3NrtJVVofRVW6j4p+e1n9MsROPNabfUy5zHO2uMCgYEAnHtLvlNpfAJ2NRC5UHBmZfia43Koe0shL9Kak/M3pqFquXX7b2ywNry25yxZSBhyKUNKafJKG4o2ghSBqwqwvLVeN0Nf5pQJS1Mqvpd1WQrmTFCTZP08MD1VT3uXq/07khwETk+m20rQ5+FYxjWSTP0z3ZHfh6qizoLQ/nrzLYcCgYAmbAKEIpc6K2DPupOeBZ0Ow0mLXPYyYaS7PTAhetMiXLM7Bf64PJ4NNVipfAnakPX60zfL1ZBjGAfsTaxYZW/Lg82woB27LYDXA13Jv/UewJUSjm3xMODfnqgwjIXAOLVLip2K13boto/Zl0GCWVzmgProMV0EBzQWGiOtC57fFQKBgCL0C4w895ACwMLDtJE6OjVG3do2b1nzAzu6cFrh9ilbXwqERyCxh38Cklx69Ip9I2sCI+oce99A9UIDMJ+zKJ1OafgwLYjEwnq33hFqpYS5QY4EZZgmVL51xMuXUN7TFu5hIbkYY64SPS2+kWTc7IwT9vTkM6kLPYihgeuGLTGbAoGAbtyMyCoW6QQq2HK+s589Qti7vYQbVXGvPCd3+gWEmye8O+hcIXOyoum5YNoCSGvsaRsQmbywokiaT1oxOu4SbKWdFBfY1kDCxCfUEPkYy4cZE2ynTtMdWsK2pu0ibFNPYd9Qlvz72FILKUlj2kylit6n2CZHJU5CAYef4ezdTRo=";
            IAopClient client = new DefaultAopClient("https://openapi.alipaydev.com/gateway.do", app_id, merchant_private_key, "json", "1.0", "RSA2", alipay_public_key, "utf-8", false);
            AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
            //异步接收地址，仅支持http/https，公网可访问
            request.SetNotifyUrl("");
            //同步跳转地址，仅支持http/https
            request.SetReturnUrl("");

            /******必传参数******/
            Dictionary<string, object> bizContent = new Dictionary<string, object>();
            //商户订单号，商家自定义，保持唯一性
            bizContent.Add("out_trade_no", a.Id);
            //支付金额，最小值0.01元
            bizContent.Add("total_amount", b);
            //订单标题，不可使用特殊符号
            bizContent.Add("subject", a.Name);
            //电脑网站支付场景固定传值FAST_INSTANT_TRADE_PAY
            bizContent.Add("product_code", "FAST_INSTANT_TRADE_PAY");

            /******可选参数******/
            //bizContent.Add("time_expire", "2022-08-01 22:00:00");

            ////商品明细信息，按需传入
            //List<object> goodsDetails = new List<object>();
            //Dictionary<string, object> goods1 = new Dictionary<string, object>();
            //goods1.Add("goods_id", "goodsNo1");
            //goods1.Add("goods_name", "子商品1");
            //goods1.Add("quantity", 1);
            //goods1.Add("price", 0.01);
            //goodsDetails.Add(goods1);
            //bizContent.Add("goods_detail", goodsDetails);

            ////扩展信息，按需传入
            //Dictionary<string, object> extendParams = new Dictionary<string, object>();
            //extendParams.Add("sys_service_provider_id", "2088501624560335");
            //bizContent.Add("extend_params", extendParams);

            string Contentjson = JsonConvert.SerializeObject(bizContent);
            request.BizContent = Contentjson;
            AlipayTradePagePayResponse response = client.pageExecute(request);
            Console.WriteLine(response.Body);
            ViewBag.Body = response.Body;
            return View();
        }

       
        public IActionResult Tijiao(int id)
        {
            var username = HttpContext.Session.GetString("user");
            var user = _userRepository.FirstOrDefault(x => x.UserName == username);
            var address = _addressRepository.FirstOrDefault(x => x.UserId == user.Id);
            var product = _productRepository.FirstOrDefault(x=>x.Id==id);
            var prourl=_producturlRepository.FirstOrDefault(x=>x.ProductId==product.Id);
            var order = _orderRepository;
            var orderdetails = _orderdetailsRepository;
            Orders orders = new Orders
            {
                UserId = user.Id,
                TotalMoney = product.Price,
                Pay = "支付宝",
                Status = 0,
                Address = address.Address1,
                CreateTime = DateTime.Now
            };
            order.Insert(orders);
            OrderDetails orderDetails = new OrderDetails
            {
                OrderId =orders.Id,
                ProductId =product.Id,
                Price = product.Price,
                Num=1,
                Remark="",
                CreateTime = DateTime.Now
            };
            orderdetails.Insert(orderDetails);
            ViewBag.Prourl=prourl;
            ViewBag.Product = product;
            ViewBag.Address = address;
            return View(user);
        }
        public IActionResult Success()
        {
          

            return View();
        }
    }
}
