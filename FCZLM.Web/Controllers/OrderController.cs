﻿using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;

namespace FZCLM.Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly IRepository<Users, int> _userRepository;
        private readonly IRepository<OrderDetails, int> _orderdetailsRepository;
        private readonly IRepository<Orders, int> _orderRepository;
        private readonly IRepository<Products, int> _productRepository;
        private readonly IRepository<ProductUrls, int> _productUrlRepository;

        public OrderController(IRepository<Users, int> userRepository,
            IRepository<Orders, int> orderRepository, IRepository<Products, int> productRepository,
            IRepository<ProductUrls, int> producturlRepository, IRepository<OrderDetails, int> orderdetailsRepository)
        {
            _userRepository = userRepository;
            _productRepository = productRepository;
            _productUrlRepository = producturlRepository;
            _orderRepository = orderRepository;
            _orderdetailsRepository = orderdetailsRepository;
        }
        public IActionResult Index()
        {
            var username = HttpContext.Session.GetString("user");
            var user = _userRepository.FirstOrDefault(x=>x.UserName==username);
            var orders = _orderRepository.GetAllList().Where(x => x.UserId == user.Id).ToList();
            var orderdetails = _orderdetailsRepository.GetAllList();
            foreach (var order in orders) 
            {
                orderdetails = _orderdetailsRepository.GetAllList().Where(x => x.OrderId == order.Id).ToList();
               
            }
            var product = _productRepository.GetAllList();
            foreach (var item in orderdetails)
            { 
                product=_productRepository.GetAllList().Where(x => x.Id == item.ProductId).ToList();
            }
            var producturl= _productUrlRepository.GetAllList();
            foreach (var item in product)
            {
                producturl = _productUrlRepository.GetAllList().Where(x => x.ProductId == item.Id).ToList();
            }
            ViewBag.Product = product;
            ViewBag.ProductUrl = producturl.First();
            ViewBag.OrderDetails = orderdetails;
            ViewBag.Orders = orders;
            return View(user);
        }
        public IActionResult Index2()
        {
            var username = HttpContext.Session.GetString("user");
            var user = _userRepository.FirstOrDefault(x => x.UserName == username);
            var orders = _orderRepository.GetAllList().Where(x => x.UserId == user.Id).ToList();
            var orderdetails = _orderdetailsRepository.GetAllList();
            foreach (var order in orders)
            {
                orderdetails = _orderdetailsRepository.GetAllList().Where(x => x.OrderId == order.Id).ToList();

            }
            var product = _productRepository.GetAllList();
            foreach (var item in orderdetails)
            {
                product = _productRepository.GetAllList().Where(x => x.Id == item.ProductId).ToList();
            }
            var producturl = _productUrlRepository.GetAllList();
            foreach (var item in product)
            {
                producturl = _productUrlRepository.GetAllList().Where(x => x.ProductId == item.Id).ToList();
            }
            ViewBag.Product = product;
            ViewBag.ProductUrl = producturl.First();
            ViewBag.OrderDetails = orderdetails;
            ViewBag.Orders = orders.Where(x=>x.Status==0).ToList();
            return View(user);
        }
        public IActionResult Index3()
        {
            var username = HttpContext.Session.GetString("user");
            var user = _userRepository.FirstOrDefault(x => x.UserName == username);
            var orders = _orderRepository.GetAllList().Where(x => x.UserId == user.Id).ToList();
            var orderdetails = _orderdetailsRepository.GetAllList();
            foreach (var order in orders)
            {
                orderdetails = _orderdetailsRepository.GetAllList().Where(x => x.OrderId == order.Id).ToList();

            }
            var product = _productRepository.GetAllList();
            foreach (var item in orderdetails)
            {
                product = _productRepository.GetAllList().Where(x => x.Id == item.ProductId).ToList();
            }
            var producturl = _productUrlRepository.GetAllList();
            foreach (var item in product)
            {
                producturl = _productUrlRepository.GetAllList().Where(x => x.ProductId == item.Id).ToList();
            }
            ViewBag.Product = product;
            ViewBag.ProductUrl = producturl.First();
            ViewBag.OrderDetails = orderdetails;
            ViewBag.Orders = orders.Where(x => x.Status == 1).ToList();
            return View(user);
        }
        public IActionResult Index4()
        {
            var username = HttpContext.Session.GetString("user");
            var user = _userRepository.FirstOrDefault(x => x.UserName == username);
            var orders = _orderRepository.GetAllList().Where(x => x.UserId == user.Id).ToList();
            var orderdetails = _orderdetailsRepository.GetAllList();
            foreach (var order in orders)
            {
                orderdetails = _orderdetailsRepository.GetAllList().Where(x => x.OrderId == order.Id).ToList();

            }
            var product = _productRepository.GetAllList();
            foreach (var item in orderdetails)
            {
                product = _productRepository.GetAllList().Where(x => x.Id == item.ProductId).ToList();
            }
            var producturl = _productUrlRepository.GetAllList();
            foreach (var item in product)
            {
                producturl = _productUrlRepository.GetAllList().Where(x => x.ProductId == item.Id).ToList();
            }
            ViewBag.Product = product;
            ViewBag.ProductUrl = producturl.First();
            ViewBag.OrderDetails = orderdetails;
            ViewBag.Orders = orders.Where(x => x.Status == 2).ToList();
            return View(user);
        }
    }
}
