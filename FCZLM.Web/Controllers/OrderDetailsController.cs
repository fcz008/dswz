﻿using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;

namespace FZCLM.Web.Controllers
{
    public class OrderDetailsController : Controller
    {
        private readonly IRepository<Users, int> _userRepository;
        private readonly IRepository<OrderDetails, int> _orderdetailsRepository;
        private readonly IRepository<Orders, int> _orderRepository;
        private readonly IRepository<Products, int> _productRepository;
        
        public OrderDetailsController(IRepository<Users, int> userRepository,
            IRepository<Orders, int> orderRepository, IRepository<Products, int> productRepository,
             IRepository<OrderDetails, int> orderdetailsRepository)
        {
            _userRepository = userRepository;
            _productRepository = productRepository;
           
            _orderRepository = orderRepository;
            _orderdetailsRepository = orderdetailsRepository;
        }
        public IActionResult Index()
        {

            var username = HttpContext.Session.GetString("user");
            var user = _userRepository.FirstOrDefault(x => x.UserName == username);
            var orders = _orderRepository.GetAllList().Where(x => x.UserId == user.Id).ToList();
            var orderdetails = _orderdetailsRepository.FirstOrDefault(x=>x.Order.UserId==user.Id);
            var product = _productRepository.FirstOrDefault(x => x.Id==orderdetails.ProductId);
            ViewBag.Product= product;
           
            ViewBag.Orders = orders;
            return View(orderdetails);
        }
    }
}
