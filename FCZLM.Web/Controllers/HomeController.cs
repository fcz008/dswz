﻿
using FCZLM.Application.Service;
using FCZLM.Application.Service.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace FZCLM.Web.Controllers
{
    public class HomeController : Controller

    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRepository<Products, int> _ProductsRepository;
        private readonly IRepository<ProductCategorys, int> _ProductCategorysRepository;
        private readonly IRepository<ProductUrls, int> _ProductUrlsRepository;
        private readonly IRepository<LunBoTu, int> _LunBoTuRepository;
        private readonly IRepository<ProductWeight, int> _ProductWeightRepository;
       

        public HomeController(ILogger<HomeController> logger, IRepository<Products, int> ProductsRepository,
            IRepository<ProductCategorys, int> ProductCategorysRepository, IRepository<ProductUrls, int> ProductUrlsRepository,  IRepository<LunBoTu,int> LunBoTuRepository, IRepository<ProductWeight, int> ProductWeightRepository)
        {
            _ProductsRepository = ProductsRepository;
            _ProductCategorysRepository = ProductCategorysRepository;
            _ProductUrlsRepository= ProductUrlsRepository;
            
            _LunBoTuRepository=LunBoTuRepository;
            _ProductWeightRepository=ProductWeightRepository;

        }

        public IActionResult Index()
        {
            var pro= _ProductsRepository.GetAllList();
            var procategory = _ProductCategorysRepository.GetAllList();
            var prourl = _ProductUrlsRepository.GetAllList();
            var lunbotu= _LunBoTuRepository.GetAllList();
            var tuijian= _ProductWeightRepository.GetAllList();
            ViewBag.ProductWeight = tuijian;
            ViewBag.LunBoTu=lunbotu;
            ViewBag.ProductCategory = procategory;
            ViewBag.ProductUrl = prourl;
            ViewBag.Product = pro;
            return View();
            
        }
       
        }
}