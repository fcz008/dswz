﻿using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FCZLM.Web.Controllers
{
    public class ContactController : Controller
    {
        private readonly IRepository<About, int> _aboutRepository;

        public ContactController(IRepository<About, int> aboutRepository)
        {
            _aboutRepository = aboutRepository;
        }
        public IActionResult Index()
        {
            var about = _aboutRepository.FirstOrDefault(x => 1 == 1);

            return View(about);
        }
    }
}
