﻿using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;

namespace FCZLM.Web.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly IRepository<Users, int> _userRepository;
        private readonly IRepository<ShoppingCart, int> _shoppingcartPepository;
        private readonly IRepository<Products, int> _productRepository;
        private readonly IRepository<ProductUrls, int> _productUrlRepository;
      

        public ShoppingCartController(IRepository<Users, int> userRepository,
            IRepository<Products, int> productRepository,
            IRepository<ProductUrls, int> producturlRepository,
            IRepository<ShoppingCart, int> shoppingcartRepository)
        {
            _userRepository = userRepository;
            _productRepository = productRepository;
            _productUrlRepository = producturlRepository;
            _shoppingcartPepository = shoppingcartRepository;
           
        }
        public IActionResult Index(int id)
        {
            var username = HttpContext.Session.GetString("user");
            var user = _userRepository.FirstOrDefault(x => x.UserName == username);
            var shoppingcart = _shoppingcartPepository.GetAllList().Where(x => x.UserId == user.Id).ToList();
            var product = _productRepository.GetAllList().Where(x=>x.Id==id).ToList();
            foreach (var proid in shoppingcart)
            {
              product.Where(x => x.Id == proid.ProductId).ToList();
            }
            var producturl = _productUrlRepository.GetAllList();
            foreach (var prourl in product)
            {
                producturl.Where(x=>x.ProductId==prourl.Id).ToList();
            }
            var a = producturl.Count();
            producturl.Distinct().ToList();
            var b= producturl.Count();
            ViewBag.ShoppingCart=shoppingcart;
            ViewBag.Product=product;
            ViewBag.ProductUrl=producturl;
            return View(product);
        }
    }
}
