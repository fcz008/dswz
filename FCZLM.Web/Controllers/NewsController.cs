﻿using FCZLM.Application.Service;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace FZCLM.Web.Controllers
{
    public class NewsController : Controller
    {
        private readonly IRepository<News, int> _newsRepository;
        private readonly IRepository<NewCategorys, int> _newCategorysRepository;
        public NewsController(ILogger<NewsController> logger, IRepository<News, int> newsRepository,
           IRepository<NewCategorys, int> newCategorysRepository)
        {
           _newsRepository = newsRepository;
           _newCategorysRepository = newCategorysRepository;
        }
        public IActionResult Index()
        {
            var news = _newsRepository.GetAllList();
            var newcategory = _newCategorysRepository.GetAllList();
            ViewBag.news = news;
            ViewBag.newCategory = newcategory;
            return View();
        }
        public IActionResult Index2(int id)
        {
            var news = _newsRepository.GetAllList().Where(x=>x.NewCategoryId==id).ToList();
            var newcategory = _newCategorysRepository.GetAllList();
            ViewBag.news = news;
            ViewBag.newCategory = newcategory;
            return View();
        }

    }
}
