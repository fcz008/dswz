﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using FZCLM.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace FCZLM.Web.Controllers
{
    public class NewsDetailsController : Controller
    {
        private readonly IRepository<News, int> _newsRepository;
        private readonly IRepository<NewCategorys, int> _newCategorysRepository;
        public NewsDetailsController( IRepository<News, int> newsRepository,
           IRepository<NewCategorys, int> newCategorysRepository)
        {
            _newsRepository = newsRepository;
            _newCategorysRepository = newCategorysRepository;
        }
        public IActionResult Index(int id)
        {
            var news = _newsRepository.FirstOrDefault(x=>x.Id==id);
            
           
            return View(news);
        }
    }
}
