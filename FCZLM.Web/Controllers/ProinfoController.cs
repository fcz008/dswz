﻿using FCZLM.Application.Service.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FCZLM.Web.Controllers
{
    public class ProinfoController : Controller
    {
        private readonly IRepository<Users, int> _userRepository;
        private readonly IRepository<Products, int> _ProductsRepository;
        private readonly IRepository<ProductCategorys, int> _ProductCategorysRepository;
        private readonly IRepository<ProductUrls, int> _ProductUrlsRepository;
        private readonly IRepository<ProductMessage, int> _ProductMessageRepository;
        public ProinfoController(ILogger<ProinfoController> logger, IRepository<Products, int> ProductsRepository,
            IRepository<ProductCategorys, int> ProductCategorysRepository, IRepository<ProductUrls, int> ProductUrlsRepository, IRepository<ProductMessage, int> ProductMessageRepository, IRepository<Users, int> userRepository)
        {
            _ProductsRepository = ProductsRepository;
            _ProductCategorysRepository = ProductCategorysRepository;
            _ProductUrlsRepository = ProductUrlsRepository;
            _ProductMessageRepository = ProductMessageRepository;
          

        }
        public IActionResult Index(int Id)
        {
            var username = HttpContext.Session.GetString("user");
            if (username == null)
            {
                RedirectToAction("index", "Login");
            }
            else 
            {
                RedirectToAction("tijiao","ZhiFu");
            }
            var pro = _ProductsRepository.GetAllList();
            var procategory = _ProductCategorysRepository.GetAllList();
            var proMessage = _ProductMessageRepository.GetAllList();
            var prourl = _ProductUrlsRepository. FirstOrDefault(x => x.Id == Id);
            ViewBag.ProductUrl = prourl;
            ViewBag.ProductMessage = proMessage;
            ViewBag.ProductCategory = procategory;
            ViewBag.Product = pro;

            return View(prourl);
           
        }
       
    }
}
