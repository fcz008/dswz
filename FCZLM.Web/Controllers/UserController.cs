﻿using FCZLM.Application.Service;
using FCZLM.Application.Service.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using FZCLM.Web.Controllers;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Net;
using System.Security.Claims;

namespace FCZLM.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IRepository<Users, int> _userRepository;
        private readonly IRepository<Address, int> _addressRepository;
        
        public UserController( IRepository<Users, int> userRepository,IRepository<Address,int> addressRepository)
        {
            _userRepository = userRepository;
            _addressRepository = addressRepository;
         
        }

        public IActionResult Index()
        {
            var username = HttpContext.Session.GetString("user");

            if (username == null)
            {

                RedirectToAction("Index", "Login");

            }
            
            var user = _userRepository.FirstOrDefault(x => x.UserName == username);
            var useraddress = _addressRepository.FirstOrDefault(x=>x.Name==user.UserName);
            ViewBag.UserAddress = useraddress.Address1;
            return View(user);
        }
       
    }
}
