﻿using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FCZLM.Web.Controllers
{

    public class ProductController : Controller
    {
        private readonly IRepository<Products, int> _ProductsRepository;
        private readonly IRepository<ProductCategorys, int> _ProductCategorysRepository;
        private readonly IRepository<ProductUrls, int> _ProductUrlsRepository;
        public ProductController(ILogger<ProductController> logger, IRepository<Products, int> ProductsRepository,
         IRepository<ProductCategorys, int> ProductCategorysRepository, IRepository<ProductUrls, int> ProductUrlsRepository
         )
        {
            _ProductsRepository = ProductsRepository;
            _ProductCategorysRepository = ProductCategorysRepository;
            _ProductUrlsRepository = ProductUrlsRepository;


        }
        public IActionResult Index()
        {
            var pro = _ProductsRepository.GetAllList();
            var procategory = _ProductCategorysRepository.GetAllList();
            var prourl = _ProductUrlsRepository.GetAllList();
            ViewBag.ProductCategory = procategory;
            ViewBag.ProductUrl = prourl;
            ViewBag.Product = pro;
            return View();
        }
    }
}
