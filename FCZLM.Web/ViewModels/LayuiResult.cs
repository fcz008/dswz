﻿using System.Collections.Generic;

namespace FZCLM.Web.Controllers
{
    public class LayuiResult<T>
    {
        public int Code { get; set; } = 0;
        public string Msg { get; set; }
        public int Count { get; set; }
        public List<T> Data { get; set; }
    }
}
