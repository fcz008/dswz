using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using FCZLM.EntityFrameworkCore.DataRepository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NetCore.AutoRegisterDi;
using System.Linq;
using System.Reflection;


namespace FCZLM.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllersWithViews();
            services.AddDbContextPool<fczlmContext>(
                    x => x.UseSqlServer(Configuration.GetConnectionString("fczlm"))
                );
            services.AddTransient(typeof(IRepository<,>), typeof(RepositoryBase<,>));
            services.AddDistributedMemoryCache();
            services.AddSession();
            services.RegisterAssemblyPublicNonGenericClasses(Assembly.Load(Configuration["Services"])).Where(x => x.Name.EndsWith("Service")).AsPublicImplementedInterfaces();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();
            app.UseSession();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
