﻿using System;
using System.Collections.Generic;

namespace FCZLM.Application
{


    public class PagedResultDto<TEntity> : PagedSortedAndFilterInput
    {

        /// <summary>
        /// 数据总合计
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 总页数
        /// </summary>
        public int TotalPages => (int)Math.Ceiling(decimal.Divide(TotalCount, Limit));

        public List<TEntity> Data { get; set; }
        /// <summary>
        /// 是否显示上一页
        /// </summary>
        public bool ShowPrevious => Page > 1;
        /// <summary>
        /// 是否显示下一页
        /// </summary>
        public bool ShowNext => Page < TotalPages;
        /// <summary>
        /// 是否为第一页
        /// </summary>
        public bool ShowFirst => Page != 1;
        /// <summary>
        /// 是否为最后一页
        /// </summary>
        public bool ShowLast => Page != TotalPages;
    }

}
