﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FCZLM.Application
{
    public class PagedSortedAndFilterInput
    {
        /// <summary>
        /// 每页分页条数
        /// </summary>
        [Range(0, 1000)]
        public int Limit { get; set; } = 50;
        /// <summary>
        /// 当前页
        /// </summary>
        [Range(0, 1000)]
        public int Page { get; set; } = 1;

    }
}
