﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class MessageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Ask { get; set; }
        public string  AskTime { get; set; }
        public string Phone { get; set; }
        public string UserId { get; set; }
        public string Huifu { get; set; }
    }
    public class MessageService : IMessafeService
    {
        private readonly IRepository<Messages, int> _MessgeRepository;
        private readonly IRepository<Users, int> _UsersRepository;

        public MessageService(IRepository<Messages, int> MessgeRepository, IRepository<Users, int> usersRepository)
        {
            _MessgeRepository = MessgeRepository;
            _UsersRepository = usersRepository;
        }


        public PagedResultDto<MessageModel> GetPaginatedResult(MessageInput input)
        {
            var query = _MessgeRepository.GetAll().Include("User").Where(x => 1 == 1);
            if (!string.IsNullOrEmpty(input.UserName))
            {
                query = query.Where(x => x.Name.Contains(input.UserName));
            }
            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new MessageModel
                {
                    Id = x.Id,
                    Huifu = x.Huifu,
                    Ask = x.Ask,
                    AskTime = x.AskTime.ToString("yyyy-MM-dd"),
                    Phone = x.Phone,
                    UserId = x.User.UserName,
                }).ToList();

            return new PagedResultDto<MessageModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
