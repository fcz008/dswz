﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class RescourModel
    {
        public int Id { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
    }
    public class RescourService : IRescourService
    {
        private readonly IRepository<Resources, int> _rescourRepository;

        public RescourService(IRepository<Resources, int> rescourRepository)
        {
            _rescourRepository = rescourRepository;
        }


        public PagedResultDto<RescourModel> GetPaginatedResult(RescourInput input)
        {
            var query = _rescourRepository.GetAll().Where(x => 1 == 1);

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new RescourModel
                {
                    Id = x.Id,
                    Action = x.Action,
                    Controller = x.Controller,

                }).ToList();

            return new PagedResultDto<RescourModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
