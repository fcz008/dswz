﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class ShoppingCartModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string ProductId { get; set; }
        public decimal? Price { get; set; }
        public int Num { get; set; }
        public decimal? Subtotal { get; set; }

    }
    public class ShoppingCartService : IShopppingCartService
    {
        private readonly IRepository<ShoppingCart, int> _shoppingcartRepository;

        public ShoppingCartService(IRepository<ShoppingCart, int> shoppingcartRepository)
        {
            _shoppingcartRepository = shoppingcartRepository;
        }


        public PagedResultDto<ShoppingCartModel> GetPaginatedResult(ShoppingCartInput input)
        {
            var query = _shoppingcartRepository.GetAll().Include("Product").Include("User").Where(x => 1 == 1);

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new ShoppingCartModel
                {
                    Id = x.Id,
                    UserId = x.User.UserName,
                    ProductId = x.Product.Name,
                    Price = x.Price,
                    Num = x.Num,
                    Subtotal = x.Subtotal,
                }).ToList();

            return new PagedResultDto<ShoppingCartModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
