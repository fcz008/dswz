﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class NewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
        public int Clicks { get; set; }
        public string CreateTime { get; set; }
        public string Source { get; set; }
        public int Status { get; set; }
        public string NewCategoryId { get; set; }
        public string Pic { get; set; }
    }
    public class NewService : INewService
    {
        private readonly IRepository<News, int> _newRepository;
        private readonly IRepository<NewCategorys, int> _newCategoryRepository;

        public NewService(IRepository<News, int> newRepository, IRepository<NewCategorys, int> newCategoryRepository)
        {
            _newRepository = newRepository;
            _newCategoryRepository = newCategoryRepository;
        }

        public string Title { get; private set; }

        public PagedResultDto<NewModel> GetPaginatedResult(NewInput input)
        {
            var query = _newRepository.GetAll().Include("NewCategory").Where(x => 1 == 1);
            if (!string.IsNullOrEmpty(input.Title))
            {
                query = query.Where(x => x.Title.Contains(input.Title));
            }
            if (input.NewCategoryId != 0)
            {
                query = query.Where(x => x.NewCategoryId == input.NewCategoryId);
            }

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new NewModel
                {
                    Id = x.Id,
                    Title = x.Title,
                    SubTitle = x.SubTitle,
                    Summary = x.Summary,
                    Content = x.Content,
                    Clicks = x.Clicks,
                    CreateTime = x.CreateTime.ToString("yyyy-MM-dd"),
                    Source = x.Source,
                    Status = x.Status,
                    NewCategoryId = x.NewCategory.Name,
                }).ToList();

            return new PagedResultDto<NewModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
