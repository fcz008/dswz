﻿using FCZLM.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using FCZLM.Core.Models;
using FCZLM.Application.HTService.Dtos;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FCZLM.Application.HTService
{
    public class ProductUrlModel
    {
        public int Id { get; set; }
        public string PicUrl { get; set; }
        public string PicType { get; set; }
        public string CreateTime { get; set; }
        public string ProductId { get; set; }

    }
    public class ProductUrlService : IProductUrlService
    {
        private readonly IRepository<ProductUrls, int> _producturlRepository;

        public ProductUrlService(IRepository<ProductUrls, int> producturlRepository)
        {
            _producturlRepository = producturlRepository;
        }


        public PagedResultDto<ProductUrlModel> GetPaginatedResult(ProductUrlInput input)
        {
            var query = _producturlRepository.GetAll().Include("Product").Where(x => 1 == 1);

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new ProductUrlModel
                {
                    Id = x.Id,
                    PicUrl = x.PicUrl,
                    PicType = x.PicType,
                    CreateTime = x.CreateTime.ToString("yyyy-MM-dd"),
                    ProductId = x.Product.Name,
                }).ToList();

            return new PagedResultDto<ProductUrlModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
