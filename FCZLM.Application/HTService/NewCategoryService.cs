﻿using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
    public class NewCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }

    }
    public class NewCategoryService : INewCategoryService
    {
        private readonly IRepository<NewCategorys, int> _newcategoryRepository;

        public NewCategoryService(IRepository<NewCategorys, int> newcategoryRepository)
        {
            _newcategoryRepository = newcategoryRepository;
        }


        public PagedResultDto<NewCategoryModel> GetPaginatedResult(NewCategoryInput input)
        {
            var query = _newcategoryRepository.GetAll().Where(x => 1 == 1);
            if (!string.IsNullOrEmpty(input.Name))
            {
                query = query.Where(x => x.Name.Contains(input.Name));
            }
            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new NewCategoryModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Desc = x.Desc,
                }).ToList();

            return new PagedResultDto<NewCategoryModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
