﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Prices { get; set; }
        public decimal? Price { get; set; }
        public string Desc { get; set; }
        public string Brand { get; set; }
        public int Status { get; set; }
        public string CreateTime { get; set; }
        public string UpdateTime { get; set; }
        public string ProductCategoryId { get; set; }
        public int? Ext1 { get; set; }
        public string Ext2 { get; set; }

    }
    public class ProductService : IProductService
    {
        private readonly IRepository<Products, int> _productRepository;

        public ProductService(IRepository<Products, int> productRepository)
        {
            _productRepository = productRepository;
        }


        public PagedResultDto<ProductModel> GetPaginatedResult(ProductInput input)
        {
          
            var query1 = _productRepository.GetAll().Include("ProductCategory").Where(x => 1 == 1);
            //根据产品名称查询
            if (!string.IsNullOrEmpty(input.Name))
            {
                query1 = query1.Where(x => x.Name.Contains(input.Name));
            }
            //根据分类查询
            if (input.ProductCategoryId != 0)
            {
                query1 = query1.Where(x => x.ProductCategoryId == input.ProductCategoryId);
            }

            //条件
            var count = query1.Count();
            var list = query1.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new ProductModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Price = x.Price,
                    Desc = x.Desc,
                    Brand = x.Brand,
                    Status = x.Status,
                    CreateTime = x.CreateTime.ToString("yyyy-MM-dd"),
                    UpdateTime = x.UpdateTime.ToString("yyyy-MM-dd"),
                    ProductCategoryId = x.ProductCategory.Name,
                }).ToList();

            return new PagedResultDto<ProductModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
