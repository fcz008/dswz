﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class AdminRoleModel
    {
        public int Id { get; set; }
        public string AdminId { get; set; }
        public string RoleId { get; set; }
    }
    public class AdminRoleService : IAdminRoleService
    {
        private readonly IRepository<AdminRole, int> _adminroleRepository;

        public AdminRoleService(IRepository<AdminRole, int> adminroleRepository)
        {
            _adminroleRepository = adminroleRepository;
        }


        public PagedResultDto<AdminRoleModel> GetPaginatedResult(AdminRoleInput input)
        {
            var query = _adminroleRepository.GetAll().Include("Admin").Include("Role").Where(x => 1 == 1);

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new AdminRoleModel
                {
                    Id = x.Id,
                    AdminId = x.Admin.LoginName,
                    RoleId = x.Role.RoleName,

                }).ToList();

            return new PagedResultDto<AdminRoleModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
