﻿using FCZLM.Application.HTService.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService
{
    public interface IRescoureRoleService
    {
        PagedResultDto<RescoureRoleModel> GetPaginatedResult(RescoureRoleInput Input);
    }
}
