﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
    public class NewCategoryInput: PagedSortedAndFilterInput
    {
        public string Name { get; set; }

    }
}
