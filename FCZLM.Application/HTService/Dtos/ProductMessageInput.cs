﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
    public class ProductMessageInput: PagedSortedAndFilterInput
    {
        public int ProductId { get; set; } = 0;
        public string Name { get; set; }

    }
}
