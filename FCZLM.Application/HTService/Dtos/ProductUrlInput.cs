﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
    public class ProductUrlInput: PagedSortedAndFilterInput
    {
        public int ProductId { get; set; } = 0; 
    }
}
