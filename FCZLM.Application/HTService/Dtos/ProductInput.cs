﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
    public class ProductInput : PagedSortedAndFilterInput
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int ProductCategoryId { get; set; } = 0;
    }
}
