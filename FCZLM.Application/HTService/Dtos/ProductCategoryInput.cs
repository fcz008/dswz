﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
     public    class  ProductCategoryInput: PagedSortedAndFilterInput
    {
        public string Name { get; set; }
    }
}
