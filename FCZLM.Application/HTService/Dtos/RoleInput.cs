﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
    public class RoleInput: PagedSortedAndFilterInput
    {
        public string RoleName { get; set; }
    }
}
