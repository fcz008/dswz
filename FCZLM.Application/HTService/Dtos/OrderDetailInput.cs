﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
    public class OrderDetailInput: PagedSortedAndFilterInput
    {
        public int OrderId { get; set; } = 0;
    }
}
