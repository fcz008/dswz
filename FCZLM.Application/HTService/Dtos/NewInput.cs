﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
    public class NewInput : PagedSortedAndFilterInput
    {
       public int Id { get; set; }
        public string Title { get; set; }

        public int NewCategoryId { get; set; } = 0;

    }
}
