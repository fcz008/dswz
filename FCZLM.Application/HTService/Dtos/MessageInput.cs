﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService.Dtos
{
    public class MessageInput: PagedSortedAndFilterInput
    {
        public string UserName { get; set; }
    }
}
