﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class OrderDetailModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string ProductId { get; set; }
        public decimal? Price { get; set; }
        public int Num { get; set; }
        public string Remark { get; set; }
        public string CreateTime { get; set; }

    }
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IRepository<OrderDetails, int> _orderdetailsRepository;

        public OrderDetailService(IRepository<OrderDetails, int> orderdetailsRepository)
        {
            _orderdetailsRepository = orderdetailsRepository;
        }


        public PagedResultDto<OrderDetailModel> GetPaginatedResult(OrderDetailInput input)
        {
            var query = _orderdetailsRepository.GetAll().Include("Product").Where(x => 1 == 1);

            if (input.OrderId != 0)
            {
                query = query.Where(x => x.OrderId == input.OrderId);
            }

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new OrderDetailModel
                {
                    Id = x.Id,
                    OrderId = x.OrderId,
                    ProductId = x.Product.Name,
                    Price = x.Price,
                    Num = x.Num,
                    Remark = x.Remark,
                    CreateTime = x.CreateTime.ToString("yyyy-MM-dd"),
                }).ToList();

            return new PagedResultDto<OrderDetailModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
