﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class RescoureRoleModel
    {
        public int Id { get; set; }
        public string RoleId { get; set; }
        public string RescoureId { get; set; }
    }
    public class RescoureRoleService : IRescoureRoleService
    {
        private readonly IRepository<RoleResources, int> _rescourRepository;

        public RescoureRoleService(IRepository<RoleResources, int> rescourRepository)
        {
            _rescourRepository = rescourRepository;
        }


        public PagedResultDto<RescoureRoleModel> GetPaginatedResult(RescoureRoleInput input)
        {
            var query = _rescourRepository.GetAll().Include("Role").Include("Rescoure").Where(x => 1 == 1);

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new RescoureRoleModel
                {
                    Id = x.Id,
                    RoleId = x.Role.RoleName,
                    RescoureId = x.Resource.Action,

                }).ToList();

            return new PagedResultDto<RescoureRoleModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
