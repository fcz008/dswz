﻿using System;
using System.Collections.Generic;
using System.Text;
using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;

namespace FCZLM.Application.HTService
{
    public interface INewService
    {
        PagedResultDto<NewModel> GetPaginatedResult(NewInput Input);
    }
}
