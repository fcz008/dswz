﻿using FCZLM.Application.HTService.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService
{
    public interface IProductUrlService
    {
        PagedResultDto<ProductUrlModel> GetPaginatedResult(ProductUrlInput Input);
    }
}
