﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string RoleDesc { get; set; }
    }
    public class UserService : IRoleService
    {
        private readonly IRepository<Roles, int> _roleRepository;
        public UserService(IRepository<Roles, int> roleRepository)
        {
            _roleRepository = roleRepository;
        }
        public PagedResultDto<RoleModel> GetPaginatedResult(RoleInput input)
        {
            var query = _roleRepository.GetAll().Where(x => 1 == 1);

            if (!string.IsNullOrEmpty(input.RoleName))
            {
                query = query.Where(x => x.RoleName.Contains(input.RoleName));
            }

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new RoleModel
                {
                  Id = x.Id,
                  RoleName = x.RoleName,
                  RoleDesc = x.RoleDesc,

                }).ToList();

            return new PagedResultDto<RoleModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }
    }
}
