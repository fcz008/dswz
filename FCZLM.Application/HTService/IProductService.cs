﻿using FCZLM.Application.HTService.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService
{
    public interface IProductService
    {
        PagedResultDto<ProductModel> GetPaginatedResult(ProductInput Input);
    }
}

