﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class OrderModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public decimal? TotalMoney { get; set; }
        public string Pay { get; set; }
        public int Status { get; set; }
        public string Address { get; set; }
        public string  CreateTime { get; set; }
        public int? OrderDetailId { get; set; }
    }
    public class OrderService : IOrderService
    {
        private readonly IRepository<Orders, int> _rescourRepository;

        public OrderService(IRepository<Orders, int> rescourRepository)
        {
            _rescourRepository = rescourRepository;
        }


        public PagedResultDto<OrderModel> GetPaginatedResult(OrderInput input)
        {
            var query = _rescourRepository.GetAll().Include("User").Include("OrderDetail").Where(x => 1 == 1);

            if (input.UserId != 0)
            {
                query = query.Where(x => x.UserId == input.UserId);
            }

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new OrderModel
                {
                    Id = x.Id,
                    UserId = x.User.UserName,
                    TotalMoney = x.TotalMoney,
                    Pay = x.Pay,
                    Status = x.Status,
                    Address = x.Address,
                    CreateTime = x.CreateTime.ToString("yyyy-MM-dd"),
                    OrderDetailId = x.OrderDetail.Id,
                }).ToList();

            return new PagedResultDto<OrderModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
