﻿using FCZLM.Application.HTService.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.HTService
{
    public class ProdutMessageModel
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        public int OrderId { get; set; }
        public string Name { get; set; }
        public string Ask { get; set; }
        public string AskTime { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }
        public string Huifu { get; set; }

    }
    public class ProductMessageService : IProductMessageService
    {
        private readonly IRepository<ProductMessage, int> _productmessageRepository;

        public ProductMessageService(IRepository<ProductMessage, int> productmessageRepository)
        {
            _productmessageRepository = productmessageRepository;
        }


        public PagedResultDto<ProdutMessageModel> GetPaginatedResult(ProductMessageInput input)
        {
            var query = _productmessageRepository.GetAll().Include("Product").Where(x => 1 == 1);
            if (!string.IsNullOrEmpty(input.Name))
            {
                query = query.Where(x=>x.Name.Contains(input.Name));
            }
            if (input.ProductId != 0)
            {
                query = query.Where(x=>x.ProductId==input.ProductId);
            }
            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new ProdutMessageModel
                {
                    Id = x.Id,
                     ProductId = x.Product.Name,
                     OrderId = x.OrderId,
                     Name = x.Name,
                     Ask = x.Ask,
                     AskTime = x.AskTime.ToString("yyyy-MM-dd"),
                     Phone = x.Phone,
                     Huifu = x.Huifu,
                }).ToList();

            return new PagedResultDto<ProdutMessageModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
