﻿using FCZLM.Application.HTService.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.HTService
{
    public interface IRescourService
    {
        PagedResultDto<RescourModel> GetPaginatedResult(RescourInput Input);
    }
}
