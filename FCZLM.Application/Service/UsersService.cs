﻿using FCZLM.Application.Service.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.Service
{
  public class UsersModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string UserPwd { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string UserTime { get; set; }
    }

    public class UsersService:IUsersService
    {
        private readonly IRepository<Users, int> _userRepository;
        public UsersService(IRepository<Users, int> userRepository)
        {
            _userRepository = userRepository;
        }
        public PagedResultDto<UsersModel> GetPaginatedResult(UsersInput input)
        {
            var query = _userRepository.GetAll();

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new UsersModel
                {
                    Id = x.Id,
                    UserName = x.UserName,
                    UserPwd = x.UserPwd,
                    Phone = x.Phone,
                    Email = x.Email,
                    UserTime = x.UserTime.ToString("yyyy-MM-dd"),

                }).ToList();

            return new PagedResultDto<UsersModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }
    }
}
