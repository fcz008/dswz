﻿using FCZLM.Application.Service.Dtos;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCZLM.Application.Service
{
    public class AboutModel
    {
        public int Id { get; set; }
        public string Fzlc { get; set; }
        public string Fsjj { get; set; }
        public string Ppjs { get; set; }
        public string Hjxx { get; set; }
        public string Name { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
    }
    public class AboutService : IAboutService
    {
       
        private readonly IRepository<About, int> _aboutRepository;
        public AboutService(IRepository<About, int> aboutRepository)
        {
            _aboutRepository = aboutRepository;
        }
        public PagedResultDto<AboutModel> GetPaginatedResult(AboutInput input)
        {
            var query = _aboutRepository.GetAll();

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new AboutModel
                {
                    Id = x.Id,
                    Fzlc = x.Fzlc,
                    Fsjj = x.Fsjj,
                    Hjxx = x.Hjxx,
                    Ppjs = x.Ppjs,
                    Tel=x.Tel,
                    Name = x.Name,
                    Address = x.Address,
                    Email = x.Email,

                }).ToList();

            return new PagedResultDto<AboutModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }
    }
}
