﻿using DotNetCoreDemo.Service.Dtos;
using FCZLM.Application.Service.Dtos;
using FCZLM.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.Service
{
    public interface IAboutService
    {
        PagedResultDto<AboutModel> GetPaginatedResult(AboutInput Input);
    }
}
