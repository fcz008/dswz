﻿using DotNetCoreDemo.Service.Dtos;
using FCZLM.Application;
using FCZLM.Core.Models;
using XProject.Service;

namespace StudentMVC.HTService
{
    public interface IAdminService
    {
        PagedResultDto<AdminModel> GetPaginatedResult(AdminsInput Input);
    }
}
