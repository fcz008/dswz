﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCZLM.Application.Service.Dtos
{
    public class UserInput:PagedSortedAndFilterInput
    {
        public string UserName { get; set; }
        public string UserPwd { get; set; }

    }
}
