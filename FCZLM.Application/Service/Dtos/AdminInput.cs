﻿using FCZLM.Application;

namespace DotNetCoreDemo.Service.Dtos
{
    public class AdminsInput : PagedSortedAndFilterInput
    {
        public string LoginName { get; set; }
        public string LoginPwd { get; set; }
        
    }
}
