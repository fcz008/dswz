﻿using DotNetCoreDemo.Service.Dtos;
using FCZLM.Application;
using FCZLM.Core.Models;
using FCZLM.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using StudentMVC.HTService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace XProject.Service
{
    public class AdminModel {
        public int Id { get; set; }
        public string LoginName { get; set; }
        public string LoginPwd { get; set; }
     
    }
    public class AdminsService : IAdminService
    {
        private readonly IRepository<Admin, int> _adminRepository;

        public AdminsService(IRepository<Admin, int> adminRepository)
        {
            _adminRepository = adminRepository;
        }


        public  PagedResultDto<AdminModel> GetPaginatedResult(AdminsInput input)
        {
            var query = _adminRepository.GetAll().Where(x => 1 == 1);

            //条件
            var count = query.Count();
            var list = query.OrderBy(x => x.Id).Skip((input.Page - 1) * input.Limit).Take(input.Limit)
                .ToList().Select(x => new AdminModel
                {
                    Id = x.Id,
                    LoginName = x.LoginName,
                    LoginPwd = x.LoginPwd,

                }).ToList();

            return new PagedResultDto<AdminModel>
            {
                Data = list,
                TotalCount = count,
                Page = input.Page,
                Limit = input.Limit,
            };
        }

    }
}
